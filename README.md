## Setting up Sublime Text

- Install [Sublime Text 3](https://www.sublimetext.com/3)
- Install [Package Control](https://packagecontrol.io/installation)

To install packages using Package Control, press ctrl+shift+p and select **Install Package**

### Compiling SCSS
1. To compile SCSS from within Sublime Text (without having to use Scout), use Package Control to install the packages **SCSS** and **Libsass Build**
2. You can configure the settings for the output of css in the file **.libsass.json** (most of the time you can leave the default settings)
3. You'll need to press ctrl+b while a .scss file is open to compile it to css (there's no automatic option like in Scout)
