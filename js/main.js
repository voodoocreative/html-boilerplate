jQuery(document).ready(function($){
	
	// Add js class
	$('html').removeClass( 'no-js' ).addClass( 'js' );
	
	// Detect whether a user is using a touch device and/or a pointer device, based on:
	// https://medium.com/@david.gilbertson/the-only-way-to-detect-touch-with-javascript-7791a3346685
	// note: this info won't be accurate straight away on page load, it will be only after the user actually performs a touch/hover
	// but this shouldn't matter, read the full article to understand why
	var userCanTouch = false;
	var userCanHover = false;
	$(window).one( 'touchstart', function(){ userCanTouch = true; $('html').addClass( 'user-can-touch' ); });
	$(window).one( 'mouseover', function(){ userCanHover = true; $('html').addClass( 'user-can-hover' ); });
	
});
